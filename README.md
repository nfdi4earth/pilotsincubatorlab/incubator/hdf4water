# Hierarchical Data Format for Water-related Big Geodata (HDF4Water)
This project is part of the [NFDI4Earth](https://www.nfdi4earth.de/).

## Project goals
In this project, we introduce an efficient big data framework based on the modern HDF5 technology, called AtlasHDF, in which we designed lossless data mappings (immediate mapping and analysis-ready mapping) from OpenStreetMap (OSM) vector data into a single HDF5 data container to facilitate fast and flexible GeoAI applications learnt from OSM data. Since the HDF5 is included as a default dependency in most GeoAI and high performance computing (HPC) environments, the proposed AtlasHDF provides a cross-platformm and single-techonology solution of handling heterogeneous big geodata for GeoAI.

## Repository Content
This project is mainly hosted in https://github.com/tum-bgd/atlashdf, from where you can find:
- [Installation and implementation details](https://github.com/tum-bgd/atlashdf/blob/main/README.md)
- [How to install](https://github.com/tum-bgd/atlashdf/blob/main/Dockerfile)
- [How to use](https://github.com/tum-bgd/atlashdf/blob/main/HDF4Water-Tutorial.ipynb)


Beside the main repository, one can find here:
- A [project description](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/hdf4water/-/blob/master/Project_Description_HDF4Water.pdf) with general information about the HDF4Water
- [Promotion video](https://youtu.be/GIzH6io7tTk) of the software: 
- Supplementary videos, which explain the project and the use of the software ([science talk](https://youtu.be/duSZxZdHE04) and [application tutorial](https://youtu.be/kUAg2cvA3v4))


## How to install and start the tutorial notebook:

We provide a [Dockerfile](https://github.com/tum-bgd/atlashdf/blob/main/Dockerfile) in the github repository. To run it locally, one need the following command:

First clone the github:
```cmd
    git clone https://github.com/tum-bgd/atlashdf.git
```

Then build the image from /atlashdf folder:
```cmd
    docker image build -t atlashdf .

```
Run the jupyter environment interactively:

```cmd
    
    docker run -p 8888:8888 -it -v ~:/atlashdf atlashdf jupyter notebook --allow-root --ip 0.0.0.0

```


## How to cite

[![DOI](https://zenodo.org/badge/511493796.svg)](https://zenodo.org/badge/latestdoi/511493796)


## Reference

- Martin Werner and Hao Li. 2022. AtlasHDF: An Efficient Big Data Framework for GeoAI. In The 10th ACM SIGSPATIAL International Workshop on Analytics for Big Geospatial Data (BigSpatial ’22) (BigSpatial ’22), November 1, 2022, Seattle, WA, USA. ACM, New York, NY, USA, 7 pages. <https://doi.org/10.1145/3557917.3567615>

- Balthasar Teuscher, Hao Li, Xin Wang, Zhouyi Xiong & Martin Werner. (2023). tum-bgd/atlashdf: v1.0.0 (v1.0.0). Zenodo. https://doi.org/10.5281/zenodo.7562587

## Acknowledgement
This work has been funded by the German Research Foundation (NFDI4Earth,
DFG project no. 460036893, https://www.nfdi4earth.de/).

  








